package com.plyshka.compressedmachinery.utils.block

import net.fabricmc.fabric.api.dimension.v1.FabricDimensions
import net.minecraft.server.MinecraftServer
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.math.Vec2f
import net.minecraft.util.math.Vec3d
import net.minecraft.util.registry.RegistryKey
import net.minecraft.world.TeleportTarget
import net.minecraft.world.World

object WorldUtils {
    fun getWorld(server: MinecraftServer, dimensionRegistryKey: RegistryKey<World>): ServerWorld? {
        return server.getWorld(dimensionRegistryKey)
    }

    fun teleportToWorld(player: ServerPlayerEntity, pos: Vec3d, rot: Vec2f, world: RegistryKey<World>) {
        val target = TeleportTarget(pos, Vec3d.ZERO, rot.y, rot.x)
        FabricDimensions.teleport(player, getWorld(player.world.server!!, world), target)
    }

    fun teleportToWorld(player: ServerPlayerEntity, pos: Vec3d, rot: Vec2f, world: ServerWorld) {
        val target = TeleportTarget(pos, Vec3d.ZERO, rot.y, rot.x)
        FabricDimensions.teleport(player, world, target)
    }

    fun teleportToWorld(player: ServerPlayerEntity, world: ServerWorld, target: TeleportTarget) {
        FabricDimensions.teleport(player, world, target)
    }

    fun getAvailableWorldId(worlds: MutableIterable<ServerWorld>): Int {
        var worldId = 0
        do {
            val isIdPresent: Boolean =
                worlds.any { serverWorld: ServerWorld ->
                    serverWorld.registryKey.value.namespace == "compressed_machinery" &&
                            serverWorld.registryKey.value.path == "void-${worldId}"
                }
            if (isIdPresent) {
                worldId++
            }
        } while (isIdPresent)
        return worldId
    }
}