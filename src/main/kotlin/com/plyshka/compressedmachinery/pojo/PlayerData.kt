package com.plyshka.compressedmachinery.pojo

import net.minecraft.util.math.Vec2f
import net.minecraft.util.math.Vec3d
import java.util.*

data class PlayerData(
    val playerUUID: UUID,
    val returnPosition: Vec3d,
    val returnRotation: Vec2f,
)
