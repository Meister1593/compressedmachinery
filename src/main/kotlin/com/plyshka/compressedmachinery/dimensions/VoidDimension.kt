package com.plyshka.compressedmachinery.dimensions

import com.mojang.serialization.Codec
import com.plyshka.compressedmachinery.chunkgenerators.VoidChunkGenerator
import com.plyshka.compressedmachinery.common.Identifiable
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import net.minecraft.util.registry.RegistryKey
import net.minecraft.world.World
import net.minecraft.world.dimension.DimensionOptions
import net.minecraft.world.dimension.DimensionType
import net.minecraft.world.gen.chunk.ChunkGenerator

object VoidDimension : DimensionKeyed, Identifiable {
    override val identifier: Identifier = Identifier("compressed_machinery", "void")

    object Type : Identifiable {
        override val identifier: Identifier = Identifier("compressed_machinery", "void_type")
    }

    override val dimensionTypeKey: RegistryKey<DimensionType> = RegistryKey.of(
        Registry.DIMENSION_TYPE_KEY,
        Type.identifier
    )
    override val dimensionKey: RegistryKey<DimensionOptions> = RegistryKey.of(
        Registry.DIMENSION_KEY,
        identifier
    )
    override val worldKey: RegistryKey<World> = RegistryKey.of(
        Registry.WORLD_KEY,
        dimensionKey.value
    )
    override val chunkGeneratorKey: RegistryKey<Codec<out ChunkGenerator>>? = RegistryKey.of(
        Registry.CHUNK_GENERATOR_KEY,
        identifier
    )

    init {
        Registry.register(Registry.CHUNK_GENERATOR, identifier, VoidChunkGenerator.CODEC)
    }
}