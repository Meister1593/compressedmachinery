package com.plyshka.compressedmachinery.dimensions

import com.mojang.serialization.Codec
import net.minecraft.util.registry.RegistryKey
import net.minecraft.world.World
import net.minecraft.world.dimension.DimensionOptions
import net.minecraft.world.dimension.DimensionType
import net.minecraft.world.gen.chunk.ChunkGenerator

interface DimensionKeyed {
    val worldKey: RegistryKey<World>
    val dimensionKey: RegistryKey<DimensionOptions>
    val dimensionTypeKey: RegistryKey<DimensionType>
    val chunkGeneratorKey: RegistryKey<Codec<out ChunkGenerator>>?
}