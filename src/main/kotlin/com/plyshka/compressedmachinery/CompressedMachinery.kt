package com.plyshka.compressedmachinery

import com.plyshka.compressedmachinery.registry.ModBlocks
import com.plyshka.compressedmachinery.registry.ModCallbacks
import com.plyshka.compressedmachinery.registry.ModDimensions
import com.plyshka.compressedmachinery.registry.ModItems

class CompressedMachinery {
    fun init() {
        ModBlocks
        ModItems
        ModCallbacks
        ModDimensions
    }

    companion object {
        const val ModId: String = "compressed_machinery"
    }
}