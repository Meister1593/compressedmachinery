package com.plyshka.compressedmachinery.callbacks

import com.plyshka.compressedmachinery.block.CompressedMachineInsideBlock
import com.plyshka.compressedmachinery.block.CompressedMachineWallBlock
import net.fabricmc.fabric.api.event.player.AttackBlockCallback
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.world.World


object AttackBlockCallbacks {
    init {
        AttackBlockCallback.EVENT.register(AttackBlockCallback { _: PlayerEntity?, world: World, _: Hand?, hitResult: BlockPos?, _: Direction? ->
            // todo: move to compressed machine block as override
            val blockState = world.getBlockState(hitResult)
            if (blockState.block is CompressedMachineWallBlock ||
                blockState.block is CompressedMachineInsideBlock
            ) {
                return@AttackBlockCallback ActionResult.CONSUME
            }; ActionResult.PASS
        })
    }
}