package com.plyshka.compressedmachinery.callbacks

import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents
import net.minecraft.server.MinecraftServer
import net.minecraft.util.ActionResult


object ServerStartCallbacks {
    init {
        ServerLifecycleEvents.SERVER_STARTED.register(ServerLifecycleEvents.ServerStarted { server: MinecraftServer ->
            ActionResult.PASS
        })
    }
}