package com.plyshka.compressedmachinery.callbacks

import net.fabricmc.fabric.api.event.player.UseBlockCallback
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.world.World

object UseBlockCallbacks {
    init {
        UseBlockCallback.EVENT.register(UseBlockCallback { player: PlayerEntity, world: World, _: Hand?, hitResult: BlockHitResult ->
            ActionResult.PASS
        })
    }
}