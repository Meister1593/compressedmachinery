package com.plyshka.compressedmachinery.block.entity

import com.plyshka.compressedmachinery.CompressedMachinery
import com.plyshka.compressedmachinery.block.CompressedMachineInsideBlock
import net.fabricmc.fabric.api.`object`.builder.v1.block.entity.FabricBlockEntityTypeBuilder
import net.minecraft.block.BlockState
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.nbt.NbtCompound
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d
import net.minecraft.util.registry.Registry
import net.minecraft.util.registry.RegistryKey
import net.minecraft.world.World

class CompressedMachineInsideBlockEntity(
    pos: BlockPos?,
    state: BlockState?,
) : BlockEntity(compressedMachineInsideBlockEntity, pos, state) {
    var worldId: Int = -1
    private var _outsideBlockEntity: CompressedMachineBlockEntity? = null
    var outsideBlockEntity: CompressedMachineBlockEntity?
        get() {
            if (_outsideBlockEntity == null) {
                _outsideBlockEntity = world!!
                    .server!!
                    .getWorld(blockEntityWorld)!!
                    .getBlockEntity(BlockPos(blockEntityPos)) as CompressedMachineBlockEntity
            }
            return _outsideBlockEntity!!
        }
        set(value) {
            _outsideBlockEntity = value
            blockEntityPos = Vec3d(
                _outsideBlockEntity!!.pos.x.toDouble(),
                _outsideBlockEntity!!.pos.y.toDouble(),
                _outsideBlockEntity!!.pos.z.toDouble()
            )
            blockEntityWorld = _outsideBlockEntity!!.world!!.registryKey
        }
    var blockEntityPos: Vec3d? = null
    var blockEntityWorld: RegistryKey<World>? = null

    companion object {
        val compressedMachineInsideBlockEntity: BlockEntityType<CompressedMachineInsideBlockEntity> = Registry.register(
            Registry.BLOCK_ENTITY_TYPE,
            "${CompressedMachinery.ModId}:compressed_machinery_inside_block_entity",
            FabricBlockEntityTypeBuilder.create(::CompressedMachineInsideBlockEntity, CompressedMachineInsideBlock)
                .build(null)
        )
    }

    override fun writeNbt(tag: NbtCompound) {
        tag.putInt("worldId", worldId)
        tag.putDouble("compressedMachineBlockEntityPosX", blockEntityPos!!.x)
        tag.putDouble("compressedMachineBlockEntityPosY", blockEntityPos!!.y)
        tag.putDouble("compressedMachineBlockEntityPosZ", blockEntityPos!!.z)
        tag.putString("compressedMachineBlockEntityWorld", blockEntityWorld!!.value.path)
        super.writeNbt(tag)
    }

    override fun readNbt(tag: NbtCompound) {
        super.readNbt(tag)
        this.worldId = tag.getInt("worldId")
        this.blockEntityPos = Vec3d(
            tag.getDouble("compressedMachineBlockEntityPosX"),
            tag.getDouble("compressedMachineBlockEntityPosY"),
            tag.getDouble("compressedMachineBlockEntityPosZ")
        )
        this.blockEntityWorld =
            RegistryKey.of(Registry.WORLD_KEY, Identifier(tag.getString("compressedMachineBlockEntityWorld")))
    }
}