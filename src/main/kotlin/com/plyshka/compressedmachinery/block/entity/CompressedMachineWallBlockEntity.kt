package com.plyshka.compressedmachinery.block.entity

import com.plyshka.compressedmachinery.CompressedMachinery
import com.plyshka.compressedmachinery.block.CompressedMachineWallBlock
import net.fabricmc.fabric.api.`object`.builder.v1.block.entity.FabricBlockEntityTypeBuilder
import net.minecraft.block.BlockState
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.util.math.BlockPos
import net.minecraft.util.registry.Registry

class CompressedMachineWallBlockEntity(
    pos: BlockPos?,
    state: BlockState?
) : BlockEntity(compressedMachineWallBlockEntity, pos, state) {
    companion object {
        val compressedMachineWallBlockEntity: BlockEntityType<CompressedMachineWallBlockEntity> = Registry.register(
            Registry.BLOCK_ENTITY_TYPE,
            "${CompressedMachinery.ModId}:compressed_machinery_wall_block_entity",
            FabricBlockEntityTypeBuilder.create(::CompressedMachineWallBlockEntity, CompressedMachineWallBlock)
                .build(null)
        )
    }
}