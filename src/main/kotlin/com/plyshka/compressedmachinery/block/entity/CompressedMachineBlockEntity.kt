package com.plyshka.compressedmachinery.block.entity

import com.plyshka.compressedmachinery.CompressedMachinery
import com.plyshka.compressedmachinery.block.CompressedMachineBlock
import com.plyshka.compressedmachinery.pojo.PlayerData
import net.fabricmc.fabric.api.`object`.builder.v1.block.entity.FabricBlockEntityTypeBuilder
import net.minecraft.block.BlockState
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtElement
import net.minecraft.nbt.NbtList
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec2f
import net.minecraft.util.math.Vec3d
import net.minecraft.util.registry.Registry

class CompressedMachineBlockEntity(
    pos: BlockPos?,
    state: BlockState?,
) : BlockEntity(compressedMachineBlockEntity, pos, state) {
    var worldId: Int = -1
    var playersData: MutableSet<PlayerData> = mutableSetOf()

    companion object {
        val compressedMachineBlockEntity: BlockEntityType<CompressedMachineBlockEntity> = Registry.register(
            Registry.BLOCK_ENTITY_TYPE,
            "${CompressedMachinery.ModId}:compressed_machinery_block_entity",
            FabricBlockEntityTypeBuilder.create(::CompressedMachineBlockEntity, CompressedMachineBlock)
                .build(null)
        )
    }

    override fun writeNbt(tag: NbtCompound) {
        tag.putInt("worldId", worldId)

        val playersDataList = NbtList()

        playersData.forEach { playerData: PlayerData ->
            val playerCompound = NbtCompound()
            playerCompound.putUuid("uuid", playerData.playerUUID)

            playerCompound.putDouble("returnPositionX", playerData.returnPosition.x)
            playerCompound.putDouble("returnPositionY", playerData.returnPosition.y)
            playerCompound.putDouble("returnPositionZ", playerData.returnPosition.z)

            playerCompound.putFloat("returnRotationX", playerData.returnRotation.x)
            playerCompound.putFloat("returnRotationY", playerData.returnRotation.y)

            playersDataList.add(playerCompound)
        }

        tag.put("playersData", playersDataList)
        super.writeNbt(tag)
    }

    override fun readNbt(tag: NbtCompound) {
        super.readNbt(tag)
        worldId = tag.getInt("worldId")

        playersData.clear()

        val playersDataList = tag.get("playersData") as NbtList
        playersDataList.forEach { playerDataElement: NbtElement ->
            val playerData = playerDataElement as NbtCompound

            playersData.add(
                PlayerData(
                    playerData.getUuid("uuid"),
                    Vec3d(
                        playerData.getDouble("returnPositionX"),
                        playerData.getDouble("returnPositionY"),
                        playerData.getDouble("returnPositionZ")
                    ),
                    Vec2f(
                        playerData.getFloat("returnRotationX"),
                        playerData.getFloat("returnRotationY"),
                    )
                )
            )
        }
    }
}