package com.plyshka.compressedmachinery.block

import com.plyshka.compressedmachinery.CompressedMachinery
import com.plyshka.compressedmachinery.block.entity.CompressedMachineBlockEntity
import com.plyshka.compressedmachinery.common.Identifiable
import com.plyshka.compressedmachinery.item.CompressedMachineBlockItem
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block.Block
import net.minecraft.block.BlockEntityProvider
import net.minecraft.block.BlockState
import net.minecraft.block.Material
import net.minecraft.block.entity.BlockEntity
import net.minecraft.client.item.TooltipContext
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NbtCompound
import net.minecraft.sound.BlockSoundGroup
import net.minecraft.text.MutableText
import net.minecraft.text.Style
import net.minecraft.text.Text
import net.minecraft.util.Formatting
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.world.BlockView
import net.minecraft.world.World
import xyz.nucleoid.fantasy.Fantasy

object CompressedMachineBlock : Block(
    FabricBlockSettings.of(Material.METAL)
        .requiresTool()
        .strength(4f)
        .sounds(BlockSoundGroup.METAL)
), Identifiable, BlockEntityProvider {
    override val identifier = Identifier(CompressedMachinery.ModId, "compressed_machine_block")
    override fun createBlockEntity(pos: BlockPos?, state: BlockState?): BlockEntity {
        return CompressedMachineBlockEntity(pos, state)
    }

    override fun onBreak(
        world: World?,
        pos: BlockPos?,
        state: BlockState?,
        player: PlayerEntity?
    ) {
        if (world!!.isClient) {
            return
        }
        // todo: this item should have separate item entity, we need to keep track of it's existence
        val droppedItem = ItemStack(CompressedMachineBlockItem)
        val blockEntity = world.getBlockEntity(pos) as CompressedMachineBlockEntity

        if (player!!.isCreative) {
            // todo: create method that only gets us new world, and if can't find it directly - it should try to "wake"
            //  that world from dimensions folder. If it doesnt find it anyway, it should error out about this
            val worldHandle = Fantasy.get(player.server).getOrOpenPersistentWorld(
                Identifier("compressed_machinery", "void-${blockEntity.worldId}"),
                // fixme: handle this? but why we need config for this
                null
            )
            // todo: do something with players inside block while it is being destroyed
            worldHandle.delete()
        } else {
            val blockCompound = NbtCompound()
            if (blockEntity.worldId != -1) {
                blockCompound.putInt(
                    "worldId",
                    blockEntity.worldId
                )
            }

            BlockItem.setBlockEntityNbt(
                droppedItem,
                CompressedMachineBlockEntity.compressedMachineBlockEntity,
                blockCompound
            )

            dropStack(world, pos, droppedItem)
        }

        super.onBreak(world, pos, state, player)
    }

    override fun appendTooltip(
        stack: ItemStack?,
        world: BlockView?,
        tooltip: MutableList<Text>?,
        options: TooltipContext?
    ) {
        val blockCompound: NbtCompound? = BlockItem.getBlockEntityNbt(stack)
        val isWorldCreated = blockCompound != null
        val tooltipText: MutableText =
            if (isWorldCreated) {
                Text.translatable(
                    "block.compressed_machinery.compressed_machine_block.world_id_tooltip",
                    blockCompound!!.getInt("worldId")
                )
            } else {
                Text.translatable(
                    "block.compressed_machinery.compressed_machine_block.no_world_tooltip"
                )
            }
        tooltipText.style = Style.EMPTY.withColor(Formatting.GRAY)
        tooltip!!.add(tooltipText)
    }
}