package com.plyshka.compressedmachinery.block

import com.plyshka.compressedmachinery.CompressedMachinery
import com.plyshka.compressedmachinery.block.entity.CompressedMachineInsideBlockEntity
import com.plyshka.compressedmachinery.common.Identifiable
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block.Block
import net.minecraft.block.BlockEntityProvider
import net.minecraft.block.BlockState
import net.minecraft.block.Material
import net.minecraft.block.entity.BlockEntity
import net.minecraft.sound.BlockSoundGroup
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d

object CompressedMachineInsideBlock : Block(
    FabricBlockSettings.of(Material.METAL)
        .strength(1f, 8f)
        .sounds(BlockSoundGroup.METAL)
), Identifiable, BlockEntityProvider {
    override val identifier = Identifier(CompressedMachinery.ModId, "compressed_machine_inside_block")
    override fun createBlockEntity(pos: BlockPos?, state: BlockState?): BlockEntity {
        return CompressedMachineInsideBlockEntity(pos, state)
    }

    val fixedPosition = Vec3d(100.0, 100.0, 100.0)
}