package com.plyshka.compressedmachinery.registry

import com.plyshka.compressedmachinery.item.CompressedMachineBlockItem
import com.plyshka.compressedmachinery.item.CompressedMachineInsideBlockItem
import com.plyshka.compressedmachinery.item.CompressedMachineWallBlockItem
import com.plyshka.compressedmachinery.item.PlayerCompressor
import net.minecraft.util.registry.Registry


object ModItems {
    init {
        Registry.register(Registry.ITEM, CompressedMachineBlockItem.identifier, CompressedMachineBlockItem)
        Registry.register(Registry.ITEM, CompressedMachineWallBlockItem.identifier, CompressedMachineWallBlockItem)
        Registry.register(Registry.ITEM, PlayerCompressor.identifier, PlayerCompressor)
        Registry.register(Registry.ITEM, CompressedMachineInsideBlockItem.identifier, CompressedMachineInsideBlockItem)
    }
}