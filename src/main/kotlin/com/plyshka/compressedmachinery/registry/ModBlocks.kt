package com.plyshka.compressedmachinery.registry

import com.plyshka.compressedmachinery.block.CompressedMachineBlock
import com.plyshka.compressedmachinery.block.CompressedMachineInsideBlock
import com.plyshka.compressedmachinery.block.CompressedMachineWallBlock
import com.plyshka.compressedmachinery.block.entity.CompressedMachineBlockEntity
import com.plyshka.compressedmachinery.block.entity.CompressedMachineInsideBlockEntity
import com.plyshka.compressedmachinery.block.entity.CompressedMachineWallBlockEntity
import net.minecraft.util.registry.Registry

object ModBlocks {
    init {
        Registry.register(Registry.BLOCK, CompressedMachineBlock.identifier, CompressedMachineBlock)
        Registry.register(Registry.BLOCK, CompressedMachineWallBlock.identifier, CompressedMachineWallBlock)
        Registry.register(Registry.BLOCK, CompressedMachineInsideBlock.identifier, CompressedMachineInsideBlock)
        CompressedMachineBlockEntity.compressedMachineBlockEntity
        CompressedMachineWallBlockEntity.compressedMachineWallBlockEntity
        CompressedMachineInsideBlockEntity.compressedMachineInsideBlockEntity
    }
}