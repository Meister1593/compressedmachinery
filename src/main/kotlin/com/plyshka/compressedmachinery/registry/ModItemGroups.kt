package com.plyshka.compressedmachinery.registry

import com.plyshka.compressedmachinery.CompressedMachinery
import com.plyshka.compressedmachinery.item.PlayerCompressor
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemStack
import net.minecraft.util.Identifier


object ModItemGroups {
    val COMPRESSOR_MACHINERY_GROUP: ItemGroup = FabricItemGroupBuilder.create(
        Identifier(CompressedMachinery.ModId, "compressed_machinery")
    )
        .icon { ItemStack(PlayerCompressor) }
        .build()
}