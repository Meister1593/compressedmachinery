package com.plyshka.compressedmachinery.registry

import com.plyshka.compressedmachinery.callbacks.AttackBlockCallbacks
import com.plyshka.compressedmachinery.callbacks.UseBlockCallbacks

object ModCallbacks {
    init {
        AttackBlockCallbacks
        UseBlockCallbacks
    }
}