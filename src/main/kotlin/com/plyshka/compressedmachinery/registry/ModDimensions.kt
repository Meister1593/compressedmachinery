package com.plyshka.compressedmachinery.registry

import com.plyshka.compressedmachinery.dimensions.VoidDimension

object ModDimensions {
    init {
        VoidDimension
    }
}