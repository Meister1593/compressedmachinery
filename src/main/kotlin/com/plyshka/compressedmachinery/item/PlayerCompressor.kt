package com.plyshka.compressedmachinery.item

import com.plyshka.compressedmachinery.CompressedMachinery
import com.plyshka.compressedmachinery.block.CompressedMachineInsideBlock
import com.plyshka.compressedmachinery.block.CompressedMachineWallBlock
import com.plyshka.compressedmachinery.block.entity.CompressedMachineBlockEntity
import com.plyshka.compressedmachinery.block.entity.CompressedMachineInsideBlockEntity
import com.plyshka.compressedmachinery.chunkgenerators.VoidChunkGenerator
import com.plyshka.compressedmachinery.common.Identifiable
import com.plyshka.compressedmachinery.dimensions.VoidDimension
import com.plyshka.compressedmachinery.pojo.PlayerData
import com.plyshka.compressedmachinery.registry.ModItemGroups
import com.plyshka.compressedmachinery.utils.block.WorldUtils
import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.minecraft.block.entity.BlockEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.item.ItemUsageContext
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.Identifier
import net.minecraft.util.TypedActionResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec2f
import net.minecraft.util.math.Vec3d
import net.minecraft.util.registry.Registry
import net.minecraft.world.World
import xyz.nucleoid.fantasy.Fantasy
import xyz.nucleoid.fantasy.RuntimeWorldConfig
import xyz.nucleoid.fantasy.RuntimeWorldHandle
import java.util.*

object PlayerCompressor : Item(
    FabricItemSettings()
        .group(ModItemGroups.COMPRESSOR_MACHINERY_GROUP)
        .maxCount(1)
), Identifiable {
    override val identifier = Identifier(CompressedMachinery.ModId, "player_compressor")

    var timer: Timer? = null
    var isAvailableForTeleport = true
    const val teleportTimeout = 2

    // todo: check if player tries to use compressed block inside block (FUTURE IDEA)
    override fun use(world: World?, originalPlayer: PlayerEntity?, hand: Hand?): TypedActionResult<ItemStack> {
        world!!
        originalPlayer!!
        if (world.isClient || !isAvailableForTeleport) {
            return TypedActionResult.pass(originalPlayer.getStackInHand(hand))
        }
        if (world.registryKey.value.path.startsWith("void")) {
            val insideCompressedBlockEntity = world.getBlockEntity(
                BlockPos(CompressedMachineInsideBlock.fixedPosition)
            ) as CompressedMachineInsideBlockEntity
            val outsideCompressedBlockEntity = insideCompressedBlockEntity.outsideBlockEntity
            if (outsideCompressedBlockEntity !is CompressedMachineBlockEntity) {
                WorldUtils.teleportToWorld(
                    originalPlayer as ServerPlayerEntity,
                    insideCompressedBlockEntity.blockEntityPos!!,
                    Vec2f.ZERO,
                    world.server!!.getWorld(insideCompressedBlockEntity.blockEntityWorld) as ServerWorld
                )
                return TypedActionResult.pass(originalPlayer.getStackInHand(hand))
            }
            val playerData = outsideCompressedBlockEntity.playersData.firstOrNull { playerData: PlayerData ->
                playerData.playerUUID == originalPlayer.uuid
            }
            outsideCompressedBlockEntity.playersData.remove(playerData)
            outsideCompressedBlockEntity.markDirty()
            WorldUtils.teleportToWorld(
                originalPlayer as ServerPlayerEntity,
                playerData!!.returnPosition,
                playerData.returnRotation,
                outsideCompressedBlockEntity.world as ServerWorld
            )
        }
        return TypedActionResult.pass(originalPlayer.getStackInHand(hand))
    }

    override fun useOnBlock(context: ItemUsageContext?): ActionResult {
        context!!
        if (context.world.isClient)
            return ActionResult.PASS

        if (context.world.registryKey.value.path.startsWith("void"))
            return ActionResult.PASS

        val world = context.world
        val player = context.player!!
        val blockPos = context.blockPos
        val registryManager = player.server!!.registryManager!!
        var compressedBlockEntity: BlockEntity? = world.getBlockEntity(blockPos)

        compressedBlockEntity = compressedBlockEntity as CompressedMachineBlockEntity
        var worldId: Int = compressedBlockEntity.worldId
        val newWorldConfig: RuntimeWorldConfig = RuntimeWorldConfig()
            .setDimensionType(VoidDimension.dimensionTypeKey)
            .setGenerator(
                VoidChunkGenerator(
                    registryManager.get(Registry.STRUCTURE_SET_KEY),
                    registryManager.get(Registry.BIOME_KEY)
                )
            )
        if (worldId == -1) {
            worldId = WorldUtils.getAvailableWorldId(player.server!!.worlds)
        }

        val worldHandle: RuntimeWorldHandle = Fantasy.get(player.server).getOrOpenPersistentWorld(
            Identifier("compressed_machinery", "void-${worldId}"), newWorldConfig
        )
        val modWorld: ServerWorld = WorldUtils.getWorld(player.server!!, worldHandle.registryKey)!!
        val boxDiameter = 10
        val startingPos = Vec3d(0.0, 100.0, 0.0)
        prepareWorld(
            modWorld,
            startingPos,
            boxDiameter
        )
        compressedBlockEntity.worldId = worldId
        compressedBlockEntity.playersData.add(
            PlayerData(
                player.uuid,
                player.pos,
                player.rotationClient
            )
        )
        compressedBlockEntity.markDirty()
        val insideCompressedBlockEntity = modWorld.getBlockEntity(
            BlockPos(CompressedMachineInsideBlock.fixedPosition)
        ) as CompressedMachineInsideBlockEntity
        insideCompressedBlockEntity.worldId = worldId
        insideCompressedBlockEntity.outsideBlockEntity = compressedBlockEntity
        // timer to stop from teleporting player again in a short time
        timer?.cancel()
        timer = Timer()
        timer?.scheduleAtFixedRate(object : TimerTask() {
            var i = teleportTimeout
            override fun run() {
                if (i > 0) {
                    i -= 1
                    isAvailableForTeleport = false
                } else {
                    isAvailableForTeleport = true
                }
            }
        }, 0, 1000)
        WorldUtils.teleportToWorld(
            player as ServerPlayerEntity,
            startingPos.add(Vec3d(boxDiameter / 2.0, 1.0, boxDiameter / 2.0)),
            Vec2f.ZERO,
            modWorld
        )
        return ActionResult.PASS
    }

    private fun prepareWorld(
        modWorld: ServerWorld,
        startingPos: Vec3d,
        boxDiameter: Int,
    ): ServerWorld {
        createHollowCube(modWorld, startingPos, boxDiameter)
        modWorld.setBlockState(
            BlockPos(CompressedMachineInsideBlock.fixedPosition),
            CompressedMachineInsideBlock.defaultState
        )
        return modWorld
    }

    private fun createHollowCube(
        modWorld: ServerWorld?,
        startingPos: Vec3d,
        diameter: Int,
    ) {
        (0..diameter).forEach { x ->
            (0..diameter).forEach { y ->
                (0..diameter).forEach { z ->
                    val blockPos = BlockPos(startingPos.x + x, startingPos.y + y, startingPos.z + z)
                    if (x == 0 || x == diameter - 1 || y == 0 || y == diameter - 1 || z == 0 || z == diameter - 1) {
                        modWorld!!.setBlockState(blockPos, CompressedMachineWallBlock.defaultState)
                    }
                }
            }
        }
    }
}
