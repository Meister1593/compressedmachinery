package com.plyshka.compressedmachinery.item

import com.plyshka.compressedmachinery.CompressedMachinery
import com.plyshka.compressedmachinery.block.CompressedMachineBlock
import com.plyshka.compressedmachinery.common.Identifiable
import com.plyshka.compressedmachinery.registry.ModItemGroups
import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.minecraft.block.BlockState
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemPlacementContext
import net.minecraft.util.Identifier

object CompressedMachineBlockItem : BlockItem(
    CompressedMachineBlock,
    FabricItemSettings().group(ModItemGroups.COMPRESSOR_MACHINERY_GROUP)
), Identifiable {
    override val identifier = Identifier(CompressedMachinery.ModId, "compressed_machine_block")

    // TODO: do not allow nested blocks (just yet, FUTURE IDEA)
    override fun canPlace(context: ItemPlacementContext?, state: BlockState?): Boolean {
        context!!
        if (context.world.registryKey.value.path.startsWith("void")) {
            return false
        }
        return super.canPlace(context, state)
    }
}