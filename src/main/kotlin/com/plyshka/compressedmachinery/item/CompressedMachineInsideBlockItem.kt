package com.plyshka.compressedmachinery.item

import com.plyshka.compressedmachinery.CompressedMachinery
import com.plyshka.compressedmachinery.block.CompressedMachineInsideBlock
import com.plyshka.compressedmachinery.common.Identifiable
import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.minecraft.block.BlockState
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemPlacementContext
import net.minecraft.util.Identifier

object CompressedMachineInsideBlockItem : BlockItem(
    CompressedMachineInsideBlock,
    FabricItemSettings()
), Identifiable {
    override val identifier = Identifier(CompressedMachinery.ModId, "compressed_machine_inside_block")

    override fun canPlace(context: ItemPlacementContext?, state: BlockState?): Boolean {
        return false
    }
}