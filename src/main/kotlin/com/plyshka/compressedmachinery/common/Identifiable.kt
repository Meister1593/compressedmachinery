package com.plyshka.compressedmachinery.common

import net.minecraft.util.Identifier

interface Identifiable {
    val identifier: Identifier
}