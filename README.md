# Compressed Machinery mod

## Backstory

I created this mod with goal of full "replication" (and enhance beyond) of Forge mod called "Compact Machines".

Because mod creator said that he won't port it to Fabric, I decided to make replica myself.

Since it's literally my first mod (at least with full idea and on fabric) and generally porting from forge to fabric
isn't an easy task...

I thought that copying some work over to this project isn't a good idea and started making this from scratch.

This would teach me how to use Fabric to create mods and hopefully create good replica for "Compact Machines" .

## Current status (08.08.2021)

Currently, there 3 blocks and 1 item in-game.

It's Compressed Machine entry point, walls inside that Compressed Machine (not shown in creative tab), block for storing
information about outside block from inside (not shown in creative tab) and Player Compressor.

You can craft Compressed Machine and Player Compressor like so:

![compressed machine craft](img/crafts/compressed_machine.png "Compressed machine")

![player compressor craft](img/crafts/player_compressor.png "Player compressor")

When you right-click on Compressed Machine block with Player Compressor, it will teleport you into this block.

To get out of this block, you just press right-click with Player Compressor inside that block anywhere.

If someone broke block outside, you will still be able to go outside, in the same position as either when you entered,
or on Compressed Machine block

## Setup

For setup instructions please see the [fabric wiki page](https://fabricmc.net/wiki/tutorial:setup) that relates to the
IDE that you are using.

Or if you are using Intellij IDEA just run `gradlew genSources` and import project with gradle.

After that Minecraft Client and Minecraft Server will be shown in run configurations

## License

MIT